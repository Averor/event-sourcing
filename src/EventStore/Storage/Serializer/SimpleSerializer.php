<?php declare(strict_types=1);

namespace Averor\MessageBus\EventSourcing\EventStore\Storage\Serializer;

use Averor\MessageBus\EventSourcing\Contract\DomainEvent;
use Averor\MessageBus\EventSourcing\Contract\EventSerializer;

/**
 * Class SimpleSerializer
 *
 * May be used with Events containing only scalar values, and no Metadata
 * When Event class is more complicated, external serializer must be provided
 *
 * @package Averor\MessageBus\EventSourcing\EventStore\Storage\Serializer
 * @author Averor <averor.dev@gmail.com>
 */
class SimpleSerializer implements EventSerializer
{
    /**
     * Returned array should contain keys:
     *      <string> id
     *      <string> date
     *      <string> name
     *      <array>  payload
     *
     * @param DomainEvent $event
     * @return array
     */
    public function serialize(DomainEvent $event) : array
    {
        $return = [
            'id' => $event->_id(),
            'date' => $event->_date()->format(DATE_ATOM),
            'name' => get_class($event),
            'payload' => get_object_vars($event)
        ];

        return $return;
    }

    /**
     * Provided array should contain keys:
     *      <string> id
     *      <string> date
     *      <string> name
     *      <array>  payload
     *
     * @param array $data
     * @return DomainEvent
     * @throws \ReflectionException
     */
    public function deserialize(array $data) : DomainEvent
    {
        $r = new \ReflectionClass($data['name']);

        /** @var DomainEvent $event */
        $event = $r->newInstanceWithoutConstructor();

        $id = $r->getProperty('_id');
        $id->setAccessible(true);
        $id->setValue($event, $data['id']);

        $date = $r->getProperty('_date');
        $date->setAccessible(true);
        $date->setValue($event, $data['date']);

        foreach ($data['payload'] as $key => $value) {
            $property = $r->getProperty($key);
            $property->setAccessible(true);
            $property->setValue($event, $value);
        }

        return $event;
    }
}
