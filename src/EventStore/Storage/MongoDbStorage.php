<?php declare(strict_types=1);

namespace Averor\MessageBus\EventSourcing\EventStore\Storage;

use Averor\MessageBus\Contract\MessageWithMetadata;
use Averor\MessageBus\EventSourcing\Contract\DomainEvent;
use Averor\MessageBus\EventSourcing\Contract\EventSerializer;
use Averor\MessageBus\EventSourcing\Contract\Storage;
use Averor\MessageBus\EventSourcing\Contract\Transaction;
use Averor\MessageBus\EventSourcing\Domain\DomainEventsTransaction;
use Averor\MessageBus\EventSourcing\Exception\EventStoreCommitException;
use MongoDB\Collection;
use MongoDB\Database;

/**
 * Class MongoDbStorage
 *
 * Collection structure:
 *      _id
 *      date
 *      metadata
 *      name
 *      payload
 *      version
 *      aggregateId
 *      aggregateName
 *
 * @package Averor\MessageBus\EventSourcing\EventStore\Storage
 * @author Averor <averor.dev@gmail.com>
 */
class MongoDbStorage implements Storage
{
    /** @var Database */
    protected $db;

    /** @var string */
    protected $collectionName = 'EventStore';

    /** @var EventSerializer */
    protected $serializer;

    /**
     * MongoDbStorage constructor.
     *
     * @param Database $db
     * @param EventSerializer $serializer
     * @param null|string $collectionName
     */
    public function __construct(Database $db, EventSerializer $serializer, ?string $collectionName = null)
    {
        $this->db = $db;

        if ($collectionName) {
            $this->collectionName = $collectionName;
        }

        $this->serializer = $serializer;
    }

    /**
     * @param string $id
     * @return Transaction
     */
    public function pull(string $id) : Transaction
    {
        /** @var Collection $collection */
        $collection = $this->db->selectCollection(
            $this->collectionName
        );

        $cursor = $collection->find(
            ['aggregateId' => $id],
            ['sort' => ['version' => 1]]
        );

        if (!$cursor) {
            return new DomainEventsTransaction(
                $id,
                '',
                0,
                []
            );
        }

        $name = '';
        $version = 0;
        $events = [];

        foreach ($cursor as $document) {

            $name = $document['aggregateName'];
            $version = $document['version'];

            $events[] = $this->serializer->deserialize([
                'id' => $document['_id'],
                'date' => $document['date'],
                'metadata' => $document['metadata'] ?? [],
                'name' => $document['name'],
                'payload' => $document['payload'],
            ]);
        }

        return new DomainEventsTransaction(
            $id,
            $name,
            $version,
            $events
        );
    }

    /**
     * @param int $currentVersion
     * @param Transaction $events
     * @return void
     */
    public function push(int $currentVersion, Transaction $events) : void
    {
        /** @var Collection $collection */
        $collection = $this->db->selectCollection(
            $this->collectionName
        );

        $id = $events->id();
        $name = $events->name();
        $newVersion = $events->version();

        $counter = $currentVersion;

        $documents = [];

        /** @var DomainEvent $event */
        foreach ($events as $event) {

            $serialized = $this->serializer->serialize(
                $event
            );

            $documents[]  = [
                '_id' => $serialized['id'],
                'date' => $serialized['date'],
                'metadata' => $serialized['metadata'] ?? [],
                'name' => $serialized['name'],
                'payload' => $serialized['payload'],
                'version' => ++$counter,
                'aggregateId' => $id,
                'aggregateName' => $name
            ];
        }

        if ($counter !== $newVersion) {
            throw new EventStoreCommitException(sprintf(
                "(%s) Expected version mismatch %d vs expected %d",
                $name,
                $counter,
                $newVersion
            ));
        }

        $collection->insertMany(
            $documents
        );
    }

    /**
     * @param string $id
     * @return int
     */
    public function currentVersion(string $id) : int
    {
        /** @var Collection $collection */
        $collection = $this->db->selectCollection(
            $this->collectionName
        );

        $return = $collection->findOne(
            ['aggregateId' => $id],
            [
                'sort' => ['version' => -1],
                'projection' => ["version" => 1],
                'limit' => 1
            ]
        );

        return (int) $return['version'];
    }
}
