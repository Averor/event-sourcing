<?php declare(strict_types=1);

namespace Averor\MessageBus\EventSourcing\EventStore\Storage;

use Averor\MessageBus\EventSourcing\Contract\DomainEvent;
use Averor\MessageBus\EventSourcing\Contract\Storage;
use Averor\MessageBus\EventSourcing\Contract\Transaction;
use Averor\MessageBus\EventSourcing\Domain\DomainEventsTransaction;
use Averor\MessageBus\EventSourcing\Exception\EventStoreCommitException;

/**
 * Class MemoryStorage
 *
 * @package Averor\MessageBus\EventSourcing\EventStore\Storage
 * @author Averor <averor.dev@gmail.com>
 */
class MemoryStorage implements Storage
{
    /** @var array */
    protected $storage;

    public function __construct()
    {
        $this->storage = [];
    }

    /**
     * @param string $id
     * @return Transaction
     */
    public function pull(string $id) : Transaction
    {
        $rows = array_filter(
            $this->storage,
            function($entry) use ($id) {
                return $entry['id'] === $id;
            }
        );

        if (!$rows) {
            return new DomainEventsTransaction(
                $id,
                '',
                0,
                []
            );
        }

        $last = end($rows);

        return new DomainEventsTransaction(
            $id,
            $last['name'],
            $last['version'],
            array_column($rows, 'event')
        );
    }

    /**
     * @param int $currentVersion
     * @param Transaction $events
     * @return void
     */
    public function push(int $currentVersion, Transaction $events) : void
    {
        $id = $events->id();
        $name = $events->name();
        $newVersion = $events->version();

        $counter = $currentVersion;

        /** @var DomainEvent $event */
        foreach ($events as $event) {
            $this->storage[] = [
                'id' => $id,
                'name' => $name,
                'version' => ++$counter,
                'event' => $event
            ];
        }

        if ($counter !== $newVersion) {
            throw new EventStoreCommitException(sprintf(
                "(%s) Expected version mismatch %d vs expected %d",
                $name,
                $counter,
                $newVersion
            ));
        }
    }

    /**
     * @param string $id
     * @return int
     */
    public function currentVersion(string $id) : int
    {
        return max(
            array_column(
                $this->pull($id)['events'],
                'version'
            ) ?: [0]
        );
    }
}
