<?php declare(strict_types=1);

namespace Averor\MessageBus\EventSourcing\EventStore;

use Averor\MessageBus\Contract\Identifier;
use Averor\MessageBus\EventSourcing\Contract\EventStore;
use Averor\MessageBus\EventSourcing\Contract\EventStream;
use Averor\MessageBus\EventSourcing\Contract\Storage;
use Averor\MessageBus\EventSourcing\Contract\Transaction;
use Averor\MessageBus\EventSourcing\Domain\DomainEventsTransaction;
use Averor\MessageBus\EventSourcing\Domain\DomainEventStream;
use Averor\MessageBus\EventSourcing\Exception\AggregateRootNotFoundException;
use Averor\MessageBus\EventSourcing\Exception\ConcurrencyException;

/**
 * Class OptimisticLockingEventStore
 *
 * @package Averor\MessageBus\EventSourcing\EventStore
 * @author Averor <averor.dev@gmail.com>
 */
class OptimisticLockingEventStore implements EventStore
{
    /** @var Storage */
    protected $storage;

    /**
     * @param Storage $storage
     */
    public function __construct(Storage $storage)
    {
        $this->storage = $storage;
    }
    /**
     * @param Identifier $id
     * @param string $name
     * @return EventStream
     */
    public function create(Identifier $id, string $name) : EventStream
    {
        return new DomainEventStream(
            $id->toString(),
            $name,
            [],
            0
        );
    }

    /**
     * @param Identifier $id
     * @return EventStream
     */
    public function load(Identifier $id): EventStream
    {
        /** @var Transaction $transaction */
        $transaction = $this->storage->pull($id->toString());

        if ($transaction->count() === 0) {
            throw new AggregateRootNotFoundException(sprintf(
                "Aggregate Root by id (%s) not found",
                $id->toString()
            ));
        }

        return new DomainEventStream(
            $transaction->id(),
            $transaction->name(),
            $transaction->events(),
            $transaction->version()
        );
    }

    /**
     * @param EventStream $stream
     * @return void
     */
    public function commit(EventStream $stream) : void
    {
        if (!($events = $stream->newEvents())) {
            return;
        }

        $id = $stream->id();
        $currentVersion = $stream->version();
        $actualCurrentVersion = $this->storage->currentVersion($id);

        if ($actualCurrentVersion !== $currentVersion) {
            throw new ConcurrencyException(sprintf(
                "(%s) version mismatch %d vs %d",
                $stream->name(),
                $actualCurrentVersion,
                $currentVersion
            ));
        }

        $this->storage->push(
            $currentVersion,
            new DomainEventsTransaction(
                $id,
                $stream->name(),
                $stream->newVersion(),
                $events
            )
        );

        $stream->markCommited();
    }
}
