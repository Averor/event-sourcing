<?php declare(strict_types=1);

namespace Averor\MessageBus\EventSourcing\Domain;

use Ramsey\Uuid\Uuid;

/**
 * Trait DomainEventTrait
 *
 * @package Averor\MessageBus\EventSourcing\Domain
 * @author Averor <averor.dev@gmail.com>
 */
trait DomainEventTrait
{
    /** @var string */
    private $_id;

    /** @var \DateTimeInterface */
    private $_date;

    /**
     * @throws \Exception
     */
    public function __construct()
    {
        // using underscores to ensure no conflict with original message properties
        $this->_id = Uuid::uuid4()->toString();
        $this->_date = new \DateTimeImmutable('now');
    }

    /**
     * @return \DateTimeInterface
     */
    public function _date() : \DateTimeInterface
    {
        return $this->_date;
    }

    /**
     * @return string
     */
    public function _id() : string
    {
        return $this->_id;
    }
}
