<?php declare(strict_types=1);

namespace Averor\MessageBus\EventSourcing\Domain;

use Averor\MessageBus\EventSourcing\Contract\DomainEvent;
use Averor\MessageBus\EventSourcing\Contract\EventStream;
use Ds\Queue;
use Traversable;

/**
 * Class DomainEventStream
 *
 * @package Averor\MessageBus\EventSourcing\Domain
 * @author Averor <averor.dev@gmail.com>
 */
class DomainEventStream implements EventStream
{
    /** @var string */
    protected $id;

    /** @var string */
    protected $name;

    /** @var Queue */
    protected $events;

    /** @var Queue */
    protected $newEvents;

    /** @var int */
    protected $version;

    /** @var int */
    protected $newVersion;

    /**
     * @param string $id
     * @param string $name
     * @param DomainEvent[] $events
     * @param int $version
     */
    public function __construct(string $id, string $name, array $events, int $version)
    {
        $this->events = new Queue($events);
        $this->newEvents = new Queue();
        $this->name = $name;
        $this->version = $this->newVersion = $version;
        $this->id = $id;
    }

    /**
     * @param DomainEvent $event
     * @return void
     */
    public function append(DomainEvent $event) : void
    {
        $this->events->push($event);
        $this->newEvents->push($event);

        $this->newVersion++;
    }

    /**
     * @return void
     */
    public function markCommited() : void
    {
        $this->newEvents->clear();
        $this->version = $this->newVersion;
    }

    /**
     * @return DomainEvent[]
     */
    public function newEvents() : array
    {
        return $this->newEvents->toArray();
    }

    /**
     * Original events count
     *
     * @return int
     */
    public function count() : int
    {
        return $this->events->count();
    }

    /**
     * Aggregate Root ID
     *
     * @return string
     */
    public function id(): string
    {
        return $this->id;
    }

    /**
     * Aggregate Root FQCN
     *
     * @return string
     */
    public function name() : string
    {
        return $this->name;
    }

    /**
     * Original events version
     *
     * @return int
     */
    public function version() : int
    {
        return $this->version;
    }

    /**
     * Version assuming addition of new events
     *
     * @return int
     */
    public function newVersion() : int
    {
        return $this->newVersion;
    }

    /**
     * Original events iterator
     *
     * @return \Generator|Traversable
     */
    public function getIterator()
    {
        return $this->events->getIterator();
    }
}
