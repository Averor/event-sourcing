<?php declare(strict_types=1);

namespace Averor\MessageBus\EventSourcing\Domain;

use Averor\MessageBus\EventSourcing\Contract\DomainEvent;
use Averor\MessageBus\EventSourcing\Contract\Transaction;
use Ds\Queue;
use Traversable;

/**
 * Class DomainEventsTransaction
 *
 * @package Averor\MessageBus\EventSourcing\Domain
 * @author Averor <averor.dev@gmail.com>
 */
class DomainEventsTransaction implements Transaction
{
    /** @var string */
    protected $id;

    /** @var string */
    protected $name;

    /** @var int */
    protected $version;

    /** @var Queue */
    protected $events;

    /**
     * @param string $id
     * @param string $name
     * @param int $version
     * @param DomainEvent[] $events
     */
    public function __construct(string $id, string $name, int $version, array $events)
    {
        $this->id = $id;
        $this->name = $name;
        $this->version = $version;
        $this->events = new Queue();

        foreach ($events as $event) {
            $this->push($event);
        }
    }

    /**
     * @param DomainEvent $event
     * @return void
     */
    private function push(DomainEvent $event) : void
    {
        $this->events->push($event);
    }

    /**
     * @return int
     */
    public function count() : int
    {
        return $this->events->count();
    }

    /**
     * @return DomainEvent[]
     */
    public function toArray(): array
    {
        return $this->events->toArray();
    }

    /**
     * @return \Generator|Traversable
     */
    public function getIterator()
    {
        return $this->events->getIterator();
    }

    /**
     * Aggregate Root ID
     *
     * @return string
     */
    public function id() : string
    {
        return $this->id;
    }

    /**
     * Aggregate Root FQCN
     *
     * @return string
     */
    public function name() : string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function version() : int
    {
        return $this->version;
    }

    /**
     * @return DomainEvent[]
     */
    public function events() : array
    {
        return $this->events->toArray();
    }
}
