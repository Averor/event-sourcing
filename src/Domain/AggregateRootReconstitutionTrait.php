<?php declare(strict_types=1);

namespace Averor\MessageBus\EventSourcing\Domain;

use Averor\MessageBus\EventSourcing\Contract\DomainEvent;
use Averor\MessageBus\EventSourcing\Contract\EventSourcedAggregateRoot;
use Averor\MessageBus\EventSourcing\Contract\EventStream;

/**
 * Trait AggregateRootReconstitutionTrait
 *
 * @package Averor\MessageBus\EventSourcing\Domain
 * @author Averor <averor.dev@gmail.com>
 */
trait AggregateRootReconstitutionTrait
{
    /**
     * @param EventStream $events
     * @return EventSourcedAggregateRoot
     * @throws \ReflectionException
     */
    public static function reconstitute(EventStream $events) : EventSourcedAggregateRoot
    {
        /** @var EventSourcedAggregateRoot $static */
        $static = (new \ReflectionClass(static::class))
            ->newInstanceWithoutConstructor();

        /** @var DomainEvent $event */
        foreach ($events as $event) {
            $static->apply($event);
        }

        return $static;
    }
}
