<?php declare(strict_types=1);

namespace Averor\MessageBus\EventSourcing\Domain;

use Averor\MessageBus\Contract\Identifier;
use Averor\MessageBus\EventSourcing\Contract\AggregateRoot;
use Averor\MessageBus\EventSourcing\Contract\DomainEvent;
use Averor\MessageBus\EventSourcing\Contract\EventBus;
use Averor\MessageBus\EventSourcing\Contract\EventSourcedAggregateRoot;
use Averor\MessageBus\EventSourcing\Contract\EventStore;
use Averor\MessageBus\EventSourcing\Contract\EventStream;
use Averor\MessageBus\EventSourcing\Contract\Repository;
use Averor\MessageBus\EventSourcing\Exception\AggregateRootNotFoundException;

/**
 * Class EventSourcedRepository
 *
 * @package Averor\MessageBus\EventSourcing\Domain
 * @author Averor <averor.dev@gmail.com>
 */
class EventSourcedRepository implements Repository
{
    /** @var EventStore */
    protected $eventStore;

    /** @var EventBus */
    protected $eventBus;

    /**
     * @param EventStore $eventStore
     * @param EventBus $eventBus
     */
    public function __construct(EventStore $eventStore, EventBus $eventBus)
    {
        $this->eventStore = $eventStore;
        $this->eventBus = $eventBus;
    }

    /**
     * @param Identifier $id
     * @param string|null $name Optional Aggregate name
     * @return AggregateRoot|EventSourcedAggregateRoot
     * @throws AggregateRootNotFoundException
     */
    public function get(Identifier $id, ?string $name = null) : AggregateRoot
    {
        $events = $this->eventStore->load($id);

        return ($events->name())::reconstitute($events);
    }

    /**
     * @param AggregateRoot|EventSourcedAggregateRoot $aggregateRoot
     * @return void
     */
    public function save(AggregateRoot $aggregateRoot) : void
    {
        /** @var EventStream $stream */
        try {
            $stream = $this->eventStore->load(
                $aggregateRoot->getIdentifier()
            );
        } catch (AggregateRootNotFoundException $exception) {
            $stream = $this->eventStore->create(
                $aggregateRoot->getIdentifier(),
                get_class($aggregateRoot)
            );
        }

        /** @var DomainEvent $event */
        foreach ($aggregateRoot->pullEvents() as $event) {
            $stream->append($event);
        }

        $this->eventStore->commit($stream);

        /** @var DomainEvent $event */
        foreach ($stream as $event) {
            $this->eventBus->dispatch($event);
        }
    }
}
