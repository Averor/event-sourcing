<?php declare(strict_types=1);

namespace Averor\MessageBus\EventSourcing\Domain;

use Averor\MessageBus\EventSourcing\Contract\DomainEvent;
use Averor\MessageBus\EventSourcing\Exception\AggregateMethodNotFoundException;

/**
 * Trait EventProducerTrait
 *
 * @package Averor\MessageBus\EventSourcing\Domain
 * @author Averor <averor.dev@gmail.com>
 */
trait EventProducerTrait
{
    /** @var DomainEvent[] */
    protected $events = [];

    /**
     * @param DomainEvent $event
     * @throws \ReflectionException
     * @throws AggregateMethodNotFoundException
     */
    public function raise(DomainEvent $event) : void
    {
        $this->events[] = $event;
        $this->apply($event);
    }

    /**
     * @return DomainEvent[]
     */
    public function pullEvents() : array
    {
        $events = $this->events;

        $this->clearEvents();

        return $events;
    }

    /**
     * @return void
     */
    public function clearEvents() : void
    {
        $this->events = [];
    }

    /**
     * @param DomainEvent $event
     * @return void
     * @throws \ReflectionException
     */
    protected function apply(DomainEvent $event) : void
    {
        $method = sprintf(
            "apply%s",
            $eventName = (new \ReflectionClass($event))->getShortName()
        );

        if (!method_exists($this, $method)) {
            throw new AggregateMethodNotFoundException(sprintf(
                "Method (%s) needed by event (%s) not found in (%s) aggregate",
                $method,
                get_class($event),
                get_class($this)

            ));
        }

        $this->$method($event);
    }
}
