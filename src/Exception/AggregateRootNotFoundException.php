<?php declare(strict_types=1);

namespace Averor\MessageBus\EventSourcing\Exception;

/**
 * Class AggregateRootNotFoundException
 *
 * @package Averor\MessageBus\EventSourcing\Exception
 * @author Averor <averor.dev@gmail.com>
 */
class AggregateRootNotFoundException extends \RuntimeException
{
}
