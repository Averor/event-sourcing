<?php declare(strict_types=1);

namespace Averor\MessageBus\EventSourcing\Exception;

/**
 * Class EventStoreCommitException
 *
 * @package Averor\MessageBus\EventSourcing\Exception
 * @author Averor <averor.dev@gmail.com>
 */
class EventStoreCommitException extends \RuntimeException
{
}
