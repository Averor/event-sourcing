<?php declare(strict_types=1);

namespace Averor\MessageBus\EventSourcing\Contract;

/**
 * Interface Projector
 *
 * @package Averor\MessageBus\EventSourcing\Contract
 * @author Averor <averor.dev@gmail.com>
 */
interface Projector
{
}
