<?php declare(strict_types=1);

namespace Averor\MessageBus\EventSourcing\Contract;

use Averor\MessageBus\Contract\Identifier;

/**
 * Interface AggregateRoot
 *
 * @package Averor\MessageBus\EventSourcing\Contract
 * @author Averor <averor.dev@gmail.com>
 */
interface AggregateRoot
{
    /**
     * @return Identifier
     */
    public function getIdentifier() : Identifier;
}
