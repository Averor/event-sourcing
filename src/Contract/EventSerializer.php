<?php declare(strict_types=1);

namespace Averor\MessageBus\EventSourcing\Contract;

/**
 * Interface EventSerializer
 *
 * @package Averor\MessageBus\EventSourcing\Contract
 * @author Averor <averor.dev@gmail.com>
 */
interface EventSerializer
{
    /**
     * Returned array should contain keys:
     *      <string> id
     *      <string> date
     *      <array>  metadata
     *      <string> name
     *      <array>  payload
     *
     * @param DomainEvent $event
     * @return array
     */
    public function serialize(DomainEvent $event) : array;

    /**
     * Provided array should contain keys:
     *      <string> id
     *      <string> date
     *      <array>  metadata
     *      <string> name
     *      <array>  payload
     *
     * @param array $data
     * @return DomainEvent
     */
    public function deserialize(array $data) : DomainEvent;
}
