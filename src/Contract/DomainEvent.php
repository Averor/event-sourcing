<?php declare(strict_types=1);

namespace Averor\MessageBus\EventSourcing\Contract;

use Averor\MessageBus\Contract\Event;
use Averor\MessageBus\Contract\Identifier;
use Averor\MessageBus\Contract\MessageWithDate;
use Averor\MessageBus\Contract\MessageWithId;

/**
 * Interface DomainEvent
 *
 * @package Averor\MessageBus\EventSourcing\Contract
 * @author Averor <averor.dev@gmail.com>
 */
interface DomainEvent extends Event, MessageWithId, MessageWithDate
{
    /**
     * @return Identifier
     */
    public function aggregateRootId() : Identifier;
}
