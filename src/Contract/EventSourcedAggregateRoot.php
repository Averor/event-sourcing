<?php declare(strict_types=1);

namespace Averor\MessageBus\EventSourcing\Contract;

/**
 * Interface EventSourcedAggregateRoot
 *
 * @package Averor\MessageBus\Contract
 * @author Averor <averor.dev@gmail.com>
 */
interface EventSourcedAggregateRoot extends AggregateRoot
{
    /**
     * @param DomainEvent $event
     * @return void
     */
    public function raise(DomainEvent $event) : void;

    /**
     * @return DomainEvent[]
     */
    public function pullEvents() : array;

    /**
     * @return void
     */
    public function clearEvents() : void;

    /**
     * @param EventStream $events
     * @return EventSourcedAggregateRoot
     */
    public static function reconstitute(EventStream $events);
}
