<?php declare(strict_types=1);

namespace Averor\MessageBus\EventSourcing\Contract;

use Averor\MessageBus\Contract\Identifier;

/**
 * Interface EventStore
 *
 * @package Averor\MessageBus\EventSourcing\Contract
 * @author Averor <averor.dev@gmail.com>
 */
interface EventStore
{
    /**
     * @param Identifier $id
     * @param string $name
     * @return EventStream
     */
    public function create(Identifier $id, string $name) : EventStream;

    /**
     * @param Identifier $id
     * @return EventStream
     */
    public function load(Identifier $id) : EventStream;

    /**
     * @param EventStream $stream
     * @return void
     */
    public function commit(EventStream $stream)  : void;
}
