<?php declare(strict_types=1);

namespace Averor\MessageBus\EventSourcing\Contract;

use Averor\MessageBus\Contract\MessageBus;

/**
 * Interface EventBus
 *
 * @package Averor\MessageBus\EventSourcing\Contract
 * @author Averor <averor.dev@gmail.com>
 */
interface EventBus extends MessageBus
{
}
