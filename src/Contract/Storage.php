<?php declare(strict_types=1);

namespace Averor\MessageBus\EventSourcing\Contract;

/**
 * Interface Storage
 *
 * @package Averor\MessageBus\EventSourcing\Contract
 * @author Averor <averor.dev@gmail.com>
 */
interface Storage
{
    /**
     * @param string $id
     * @return Transaction
     */
    public function pull(string $id) : Transaction;

    /**
     * @param int $currentVersion
     * @param Transaction $events
     * @return void
     */
    public function push(int $currentVersion, Transaction $events) : void;

    /**
     * @param string $id
     * @return int
     */
    public function currentVersion(string $id) : int;
}
