<?php declare(strict_types=1);

namespace Averor\MessageBus\EventSourcing\Contract;

/**
 * Interface Transaction
 *
 * Main difference between Transaction and EventStream is their intent.
 * Transaction serves as immutable DTO used to transfer collection of
 * aggregate events between layers of application
 *
 * @package Averor\MessageBus\EventSourcing\Contract
 * @author Averor <averor.dev@gmail.com>
 */
interface Transaction extends \IteratorAggregate, \Countable
{
    /**
     * Aggregate Root ID
     *
     * @return string
     */
    public function id() : string;

    /**
     * Aggregate Root FQCN
     *
     * @return string
     */
    public function name() : string;

    /**
     * @return int
     */
    public function version() : int;

    /**
     * @return DomainEvent[]
     */
    public function events() : array;

    /**
     * @return int
     */
    public function count() : int;

    /**
     * @return array
     */
    public function toArray() : array;
}
