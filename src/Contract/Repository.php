<?php declare(strict_types=1);

namespace Averor\MessageBus\EventSourcing\Contract;

use Averor\MessageBus\Contract\Identifier;

/**
 * Interface Repository
 *
 * @package Averor\MessageBus\EventSourcing\Contract
 * @author Averor <averor.dev@gmail.com>
 */
interface Repository
{
    /**
     * @param Identifier $id
     * @param string|null $name Optional Aggregate name
     * @return AggregateRoot
     */
    public function get(Identifier $id, ?string $name = null) : AggregateRoot;

    /**
     * @param AggregateRoot $aggregateRoot
     * @return void
     */
    public function save(AggregateRoot $aggregateRoot) : void;
}
