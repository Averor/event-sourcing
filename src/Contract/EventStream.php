<?php declare(strict_types=1);

namespace Averor\MessageBus\EventSourcing\Contract;

/**
 * Interface EventStream
 *
 * @package Averor\MessageBus\EventSourcing\Contract
 * @author Averor <averor.dev@gmail.com>
 */
interface EventStream extends \IteratorAggregate
{
    /**
     * Adds new event to the already existing stream.
     * e.g. When AR is reconstituted and new event happened
     *
     * @param DomainEvent $event
     * @return void
     */
    public function append(DomainEvent $event) : void;

    /**
     * @return DomainEvent[]
     */
    public function newEvents() : array;

    /**
     * @return void
     */
    public function markCommited() : void;

    /**
     * Events count
     *
     * @return int
     */
    public function count() : int;

    /**
     * Aggregate Root ID
     *
     * @return string
     */
    public function id() : string;

    /**
     * Aggregate Root FQCN
     *
     * @return string
     */
    public function name() : string;

    /**
     * @return int
     */
    public function version() : int;

    /**
     * @return int
     */
    public function newVersion() : int;
}
