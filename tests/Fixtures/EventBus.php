<?php declare(strict_types=1);

namespace Averor\MessageBus\EventSourcing\Tests\Fixtures;

use Averor\MessageBus\EventSourcing\Contract\EventBus as IEventBus;
use Averor\MessageBus\MessageBus;

/**
 * Class EventBus
 *
 * @package Averor\MessageBus\EventSourcing\Tests\Fixtures
 * @author Averor <averor.dev@gmail.com>
 */
class EventBus extends MessageBus implements IEventBus
{
}
