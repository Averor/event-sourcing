<?php declare(strict_types=1);

namespace Averor\MessageBus\EventSourcing\Tests\Fixtures\Dummy;

use Averor\CqrsBundle\Domain\UuidIdentifier;
use Averor\MessageBus\Contract\Identifier;
use Averor\MessageBus\EventSourcing\Contract\EventSourcedAggregateRoot;
use Averor\MessageBus\EventSourcing\Domain\AggregateRootReconstitutionTrait;
use Averor\MessageBus\EventSourcing\Domain\EventProducerTrait;

/**
 * Class DummyAggregateRoot
 *
 * @package Averor\MessageBus\EventSourcing\Tests\Fixtures\Dummy
 * @author Averor <averor.dev@gmail.com>
 */
class DummyAggregateRoot implements EventSourcedAggregateRoot
{
    use EventProducerTrait, AggregateRootReconstitutionTrait;

    /**
     * @return Identifier
     */
    public function getIdentifier() : Identifier
    {
        return new UuidIdentifier();
    }
}
