<?php declare(strict_types=1);

namespace Averor\MessageBus\EventSourcing\Tests\Fixtures\Dummy;

use Averor\CqrsBundle\Domain\UuidIdentifier;
use Averor\MessageBus\Contract\Identifier;
use Averor\MessageBus\EventSourcing\Contract\DomainEvent;
use Averor\MessageBus\EventSourcing\Domain\DomainEventTrait;

/**
 * Class DummyDomainEvent
 *
 * @package Averor\MessageBus\EventSourcing\Tests\Fixtures\Dummy
 * @author Averor <averor.dev@gmail.com>
 */
class DummyDomainEvent implements DomainEvent
{
    use DomainEventTrait {
        DomainEventTrait::__construct as protected __DomainEventTraitConstruct;
    }

    public function aggregateRootId(): Identifier
    {
        return new UuidIdentifier();
    }
}
