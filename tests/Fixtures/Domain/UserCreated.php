<?php declare(strict_types=1);

namespace Averor\MessageBus\EventSourcing\Tests\Fixtures\Domain;

use Averor\MessageBus\Contract\Identifier;
use Averor\MessageBus\EventSourcing\Contract\DomainEvent;
use Averor\MessageBus\EventSourcing\Domain\DomainEventTrait;

/**
 * Class UserCreated
 *
 * @package Averor\MessageBus\EventSourcing\Tests\Fixtures\Domain
 * @author Averor <averor.dev@gmail.com>
 */
class UserCreated implements DomainEvent
{
    use DomainEventTrait {
        DomainEventTrait::__construct as protected __DomainEventTraitConstruct;
    }

    /** @var Identifier */
    public $id;

    /** @var string */
    public $name;

    /** @var string */
    public $email;

    /** @var string */
    public $status;

    /**
     * @param Identifier $id
     * @param string $name
     * @param string $email
     * @param string $status
     * @throws \Exception
     */
    public function __construct(Identifier $id, string $name, string $email, string $status)
    {
        $this->id = $id;
        $this->name = $name;
        $this->email = $email;
        $this->status = $status;

        $this->__DomainEventTraitConstruct();
    }

    /**
     * @return Identifier
     */
    public function aggregateRootId() : Identifier
    {
        return $this->id;
    }
}
