<?php declare(strict_types=1);

namespace Averor\MessageBus\EventSourcing\Tests\Fixtures\Domain;

use Averor\MessageBus\Contract\Identifier;
use Averor\MessageBus\EventSourcing\Contract\EventSourcedAggregateRoot;
use Averor\MessageBus\EventSourcing\Domain\AggregateRootReconstitutionTrait;
use Averor\MessageBus\EventSourcing\Domain\EventProducerTrait;

/**
 * Class User
 *
 * @package Averor\MessageBus\EventSourcing\Tests\Fixtures\Domain
 * @author Averor <averor.dev@gmail.com>
 */
class User implements EventSourcedAggregateRoot
{
    use EventProducerTrait, AggregateRootReconstitutionTrait;

    /** @var Identifier */
    private $id;

    /** @var string */
    private $name;

    /** @var string */
    private $email;

    /** @var string */
    private $status;

    public function getIdentifier() : Identifier
    {
        return $this->id;
    }

    public function name() : string
    {
        return $this->name;
    }

    public function email() : string
    {
        return $this->email;
    }

    public function status() : string
    {
        return $this->status;
    }

    public static function create(Identifier $id, string $name, string $email) : User
    {
        $user = new static();

        /** @noinspection PhpUnhandledExceptionInspection */
        $user->raise(new UserCreated(
            $id,
            $name,
            $email,
            'new'
        ));

        return $user;
    }

    public function activate() : void
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $this->raise(new UserStatusChanged(
            $this->id,
            'active'
        ));
    }

    public function deactivate() : void
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $this->raise(new UserStatusChanged(
            $this->id,
            'inactive'
        ));
    }

    public function rename(string $newName) : void
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $this->raise(new UserNameChanged(
            $this->id,
            $newName
        ));
    }

    protected function applyUserCreated(UserCreated $event) : void
    {
        $this->id = $event->id;
        $this->name = $event->name;
        $this->email = $event->email;
        $this->status = $event->status;
    }

    protected function applyUserStatusChanged(UserStatusChanged $event) : void
    {
        $this->status = $event->status;
    }
}
