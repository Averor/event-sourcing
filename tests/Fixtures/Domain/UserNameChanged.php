<?php declare(strict_types=1);

namespace Averor\MessageBus\EventSourcing\Tests\Fixtures\Domain;

use Averor\MessageBus\Contract\Identifier;
use Averor\MessageBus\EventSourcing\Contract\DomainEvent;
use Averor\MessageBus\EventSourcing\Domain\DomainEventTrait;

/**
 * Class UserNameChanged
 *
 * @package Averor\MessageBus\EventSourcing\Tests\Fixtures\Domain
 * @author Averor <averor.dev@gmail.com>
 */
class UserNameChanged implements DomainEvent
{
    use DomainEventTrait {
        DomainEventTrait::__construct as protected __DomainEventTraitConstruct;
    }

    /** @var Identifier */
    public $id;

    /** @var string */
    public $name;

    /**
     * @param Identifier $id
     * @param string $name
     * @throws \Exception
     */
    public function __construct(Identifier $id, string $name)
    {
        $this->id = $id;
        $this->name = $name;

        $this->__DomainEventTraitConstruct();
    }

    /**
     * @return Identifier
     */
    public function aggregateRootId() : Identifier
    {
        return $this->id;
    }
}
