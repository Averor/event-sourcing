<?php declare(strict_types=1);

namespace Averor\MessageBus\EventSourcing\Tests\EventStore;

use Averor\CqrsBundle\Domain\UuidIdentifier;
use Averor\MessageBus\EventSourcing\Contract\Transaction;
use Averor\MessageBus\EventSourcing\Domain\DomainEventsTransaction;
use Averor\MessageBus\EventSourcing\Domain\DomainEventStream;
use Averor\MessageBus\EventSourcing\EventStore\OptimisticLockingEventStore;
use Averor\MessageBus\EventSourcing\EventStore\Storage\MemoryStorage;
use Averor\MessageBus\EventSourcing\Exception\AggregateRootNotFoundException;
use Averor\MessageBus\EventSourcing\Exception\ConcurrencyException;
use Averor\MessageBus\EventSourcing\Tests\Fixtures\Dummy\DummyAggregateRoot;
use Averor\MessageBus\EventSourcing\Tests\Fixtures\Dummy\DummyDomainEvent;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

/**
 * Class OptimisticLockingEventStoreTest
 *
 * @package Averor\MessageBus\EventSourcing\Tests\EventStore
 * @author Averor <averor.dev@gmail.com>
 */
class OptimisticLockingEventStoreTest extends TestCase
{
    public function test_it_creates_new_event_stream_correctly() : void
    {
        /** @noinspection PhpParamsInspection */
        $eventStore = new OptimisticLockingEventStore(
            $this->createMock(MemoryStorage::class)
        );

        $stream = $eventStore->create($id = new UuidIdentifier(), $name = DummyAggregateRoot::class);

        $this->assertEquals(
            new DomainEventStream(
                $id->toString(),
                $name,
                [],
                0
            ),
            $stream
        );
    }

    public function test_it_loads_event_stream_correctly() : void
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $stream = new DomainEventStream(
            ($id = new UuidIdentifier())->toString(),
            $name = DummyAggregateRoot::class,
            $events = [new DummyDomainEvent(), new DummyDomainEvent()],
            $version = 2
        );

        $storage = $this->createMock(MemoryStorage::class);
        $storage->method('pull')->willReturn(
            new DomainEventsTransaction(
                $id->toString(),
                $name,
                $version,
                $events
            )
        );

        /** @noinspection PhpParamsInspection */
        $eventStore = new OptimisticLockingEventStore($storage);

        $this->assertEquals(
            $stream,
            $eventStore->load($id)
        );
    }

    public function test_it_throws_exception_when_trying_to_load_by_unknown_id() : void
    {
        $this->expectException(AggregateRootNotFoundException::class);

        $storage = $this->createMock(MemoryStorage::class);
        $storage->method('pull')->willReturn(
            new DomainEventsTransaction(
                (new UuidIdentifier())->toString(),
                '',
                0,
                []
            )
        );

        /** @noinspection PhpParamsInspection */
        $eventStore = new OptimisticLockingEventStore($storage);
        $eventStore->load(new UuidIdentifier());
    }

    public function test_it_returns_gracefully_without_doing_anything_when_no_new_events_to_be_stored() : void
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $stream = new DomainEventStream(
            ($id = new UuidIdentifier())->toString(),
            $name = DummyAggregateRoot::class,
            $events = [new DummyDomainEvent(), new DummyDomainEvent()],
            $version = 2
        );

        $storage = $this->prophesize(MemoryStorage::class);
        /** @noinspection PhpUndefinedMethodInspection */
        $storage->push(Argument::any())->shouldNotBeCalled();

        /** @noinspection PhpParamsInspection */
        $eventStore = new OptimisticLockingEventStore($storage->reveal());
        $eventStore->commit($stream);
    }

    public function test_it_commits_stream_correctly_by_pushing_to_storage() : void
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $stream = new DomainEventStream(
            ($id = new UuidIdentifier())->toString(),
            $name = DummyAggregateRoot::class,
            $events = [new DummyDomainEvent()],
            $version = 1
        );

        /** @noinspection PhpUnhandledExceptionInspection */
        $stream->append(new DummyDomainEvent());

        /** @noinspection PhpUnhandledExceptionInspection */
        $stream->append(new DummyDomainEvent());

        $storage = $this->prophesize(MemoryStorage::class);
        /** @noinspection PhpUndefinedMethodInspection */
        $storage->currentVersion($id->toString())->shouldBeCalled()->willReturn($version);
        /** @noinspection PhpUndefinedMethodInspection */
        $storage->push($version, Argument::type(Transaction::class))->shouldBeCalled();

        /** @noinspection PhpParamsInspection */
        $eventStore = new OptimisticLockingEventStore($storage->reveal());
        $eventStore->commit($stream);
    }

    public function test_it_throws_concurrency_exception_upon_version_mismatch() : void
    {
        $this->expectException(ConcurrencyException::class);

        /** @noinspection PhpUnhandledExceptionInspection */
        $stream = new DomainEventStream(
            ($id = new UuidIdentifier())->toString(),
            $name = DummyAggregateRoot::class,
            $events = [new DummyDomainEvent()],
            $version = 1
        );

        /** @noinspection PhpUnhandledExceptionInspection */
        $stream->append(new DummyDomainEvent());

        $storage = $this->prophesize(MemoryStorage::class);
        /** @noinspection PhpUndefinedMethodInspection */
        $storage->currentVersion($id->toString())->shouldBeCalled()->willReturn(2);
        /** @noinspection PhpUndefinedMethodInspection */
        $storage->push(Argument::any(), Argument::any())->shouldNotBeCalled();

        /** @noinspection PhpParamsInspection */
        $eventStore = new OptimisticLockingEventStore($storage->reveal());
        $eventStore->commit($stream);
    }
}
