<?php declare(strict_types=1);

namespace Averor\MessageBus\EventSourcing\Tests\Domain;

use Averor\CqrsBundle\Domain\UuidIdentifier;
use Averor\MessageBus\EventSourcing\Contract\DomainEvent;
use Averor\MessageBus\EventSourcing\Contract\EventBus;
use Averor\MessageBus\EventSourcing\Contract\EventStream;
use Averor\MessageBus\EventSourcing\Domain\DomainEventStream;
use Averor\MessageBus\EventSourcing\Domain\EventSourcedRepository;
use Averor\MessageBus\EventSourcing\EventStore\OptimisticLockingEventStore;
use Averor\MessageBus\EventSourcing\Exception\AggregateRootNotFoundException;
use Averor\MessageBus\EventSourcing\Tests\Fixtures\Domain\User;
use Averor\MessageBus\EventSourcing\Tests\Fixtures\Domain\UserCreated;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

/**
 * Class EventSourcedRepositoryTest
 *
 * @package Averor\MessageBus\EventSourcing\Tests\Domain
 * @author Averor <averor.dev@gmail.com>
 */
class EventSourcedRepositoryTest extends TestCase
{
    public function test_it_gets_aggregate_from_store() : void
    {
        $id = new UuidIdentifier();

        /** @noinspection PhpUnhandledExceptionInspection */
        $stream = new DomainEventStream(
            $id->toString(),
            User::class,
            [
                new UserCreated($id, $name = 'John Doe', $email = 'johndoe@example.com', 'new'),
            ],
            1
        );

        $eventBus = $this->createMock(EventBus::class);

        $eventStore = $this->createMock(OptimisticLockingEventStore::class);
        $eventStore->method('load')
            ->with($id)
            ->willReturn($stream);

        /** @noinspection PhpParamsInspection */
        $repository = new EventSourcedRepository($eventStore, $eventBus);

        /** @var User $user */
        $user = $repository->get($id);

        $this->assertEquals($id->toString(), $user->getIdentifier()->toString());
        $this->assertEquals($name, $user->name());
        $this->assertEquals($email, $user->email());
    }

    public function test_it_throws_not_found_exception_when_unknown_id() : void
    {
        $this->expectException(AggregateRootNotFoundException::class);

        $eventStore = $this->createMock(OptimisticLockingEventStore::class);
        $eventStore->method('load')
            ->with()
            ->willThrowException(new AggregateRootNotFoundException());

        /** @noinspection PhpParamsInspection */
        $repository = new EventSourcedRepository(
            $eventStore,
            $this->createMock(EventBus::class)
        );

        $repository->get(new UuidIdentifier());
    }

    public function test_it_saves_new_aggregate_in_store() : void
    {
        $eventStore = $this->prophesize(OptimisticLockingEventStore::class);

        /** @noinspection PhpUndefinedMethodInspection */
        $eventStore->load(Argument::type(UuidIdentifier::class))
            ->shouldBeCalled()
            ->willThrow(AggregateRootNotFoundException::class);

        /** @noinspection PhpUndefinedMethodInspection */
        $eventStore->create(
            Argument::type(UuidIdentifier::class),
            Argument::type('string')
        )
            ->will(function ($args) {
                /** @noinspection PhpUndefinedMethodInspection */
                return new DomainEventStream(
                    $args[0]->toString(),
                    $args[1],
                    [],
                    0
                );
            })
            ->shouldBeCalled();

        /** @noinspection PhpUndefinedMethodInspection */
        $eventStore->commit(Argument::type(EventStream::class))->shouldBeCalled();

        /** @noinspection PhpParamsInspection */
        $repository = new EventSourcedRepository(
            $eventStore->reveal(),
            $this->createMock(EventBus::class)
        );

        /** @noinspection PhpUnhandledExceptionInspection */
        $repository->save(
            User::create(new UuidIdentifier(), 'John Doe', 'johndoe@example.com')
        );
    }

    public function test_it_saves_reconstituted_aggregate_in_store() : void
    {
        $id = new UuidIdentifier();

        $eventStore = $this->prophesize(OptimisticLockingEventStore::class);

        /** @noinspection PhpUndefinedMethodInspection */
        $eventStore->load(Argument::type(UuidIdentifier::class))
            ->will(function ($args) use($id) {
                /** @noinspection PhpUndefinedMethodInspection */
                return new DomainEventStream(
                    $args[0]->toString(),
                    User::class,
                    [
                        new UserCreated($id, 'John Doe', 'johndoe@example.com', 'new')
                    ],
                    0
                );
            })
            ->shouldBeCalledTimes(2);

        /** @noinspection PhpUndefinedMethodInspection */
        $eventStore->create()->shouldNotBeCalled();

        /** @noinspection PhpUndefinedMethodInspection */
        $eventStore->commit(Argument::type(EventStream::class))->shouldBeCalled();

        /** @noinspection PhpParamsInspection */
        $repository = new EventSourcedRepository(
            $eventStore->reveal(),
            $this->createMock(EventBus::class)
        );

        /** @var User $user */
        $user = $repository->get($id);
        $user->activate();

        /** @noinspection PhpUnhandledExceptionInspection */
        $repository->save($user);
    }

    public function test_it_dispatches_events_via_event_bus() : void
    {
        $eventStore = $this->prophesize(OptimisticLockingEventStore::class);

        /** @noinspection PhpUndefinedMethodInspection */
        $eventStore->load(Argument::type(UuidIdentifier::class))
            ->shouldBeCalled()
            ->willThrow(AggregateRootNotFoundException::class);

        /** @noinspection PhpUndefinedMethodInspection */
        $eventStore->create(
            Argument::type(UuidIdentifier::class),
            Argument::type('string')
        )
            ->will(function ($args) {
                /** @noinspection PhpUndefinedMethodInspection */
                return new DomainEventStream(
                    $args[0]->toString(),
                    $args[1],
                    [],
                    0
                );
            })
            ->shouldBeCalled();

        /** @noinspection PhpUndefinedMethodInspection */
        $eventStore->commit(Argument::type(EventStream::class))->shouldBeCalled();

        // Create a prophecy for the Observer class.
        $eventBus = $this->prophesize(EventBus::class);
        $eventBus->dispatch(Argument::type(DomainEvent::class))->shouldBeCalled();

        /** @noinspection PhpParamsInspection */
        $repository = new EventSourcedRepository(
            $eventStore->reveal(),
            $eventBus->reveal()
        );

        /** @noinspection PhpUnhandledExceptionInspection */
        $repository->save(
            User::create(new UuidIdentifier(), 'John Doe', 'johndoe@example.com')
        );
    }
}
