<?php declare(strict_types=1);

namespace Averor\MessageBus\EventSourcing\Tests\Domain;

use Averor\CqrsBundle\Domain\UuidIdentifier;
use Averor\MessageBus\EventSourcing\Tests\Fixtures\Domain\UserCreated;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;

/**
 * Class DomainEventTest
 *
 * @package Averor\MessageBus\EventSourcing\Tests\Domain
 * @author Averor <averor.dev@gmail.com>
 */
class DomainEventTest extends TestCase
{
    public function test_event_metadata() : void
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $event = new UserCreated(
            new UuidIdentifier(),
            'foo',
            'bar@example.com',
            'new'
        );

        $this->assertInstanceOf(\DateTimeInterface::class, $event->_date());
        $this->assertRegExp('/'.Uuid::VALID_PATTERN.'/', $event->_id());
    }
}
