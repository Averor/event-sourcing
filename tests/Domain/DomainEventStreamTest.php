<?php declare(strict_types=1);

namespace Averor\MessageBus\EventSourcing\Tests\Domain;

use Averor\CqrsBundle\Domain\UuidIdentifier;
use Averor\MessageBus\EventSourcing\Contract\DomainEvent;
use Averor\MessageBus\EventSourcing\Contract\EventStream;
use Averor\MessageBus\EventSourcing\Domain\DomainEventStream;
use Averor\MessageBus\EventSourcing\Tests\Fixtures\Domain\User;
use Averor\MessageBus\EventSourcing\Tests\Fixtures\Dummy\DummyDomainEvent;
use PHPUnit\Framework\TestCase;

/**
 * Class DomainEventStreamTest
 *
 * @package Averor\MessageBus\EventSourcing\Tests\Domain
 * @author Averor <averor.dev@gmail.com>
 */
class DomainEventStreamTest extends TestCase
{
    public function test_it_allows_iterating_over_events() : void
    {
        $stream = $this->generateStream();

        /** @noinspection PhpUnhandledExceptionInspection */
        $stream->append(new DummyDomainEvent());
        /** @noinspection PhpUnhandledExceptionInspection */
        $stream->append(new DummyDomainEvent());
        /** @noinspection PhpUnhandledExceptionInspection */
        $stream->append(new DummyDomainEvent());

        $count = 0;
        foreach ($stream as $event) {
            $count++;
            $this->assertInstanceOf(DomainEvent::class, $event);
        }

        $this->assertEquals(3, $count);
    }

    public function test_appending_new_events_does_not_change_version() : void
    {
        $stream = $this->generateStream();

        /** @noinspection PhpUnhandledExceptionInspection */
        $stream->append(new DummyDomainEvent());
        /** @noinspection PhpUnhandledExceptionInspection */
        $stream->append(new DummyDomainEvent());
        /** @noinspection PhpUnhandledExceptionInspection */
        $stream->append(new DummyDomainEvent());

        $this->assertEquals(0, $stream->version());
    }

    public function test_appending_new_events_causes_correct_new_version() : void
    {
        $stream = $this->generateStream();

        /** @noinspection PhpUnhandledExceptionInspection */
        $stream->append(new DummyDomainEvent());
        /** @noinspection PhpUnhandledExceptionInspection */
        $stream->append(new DummyDomainEvent());
        /** @noinspection PhpUnhandledExceptionInspection */
        $stream->append(new DummyDomainEvent());

        $this->assertEquals(3, $stream->newVersion());
    }

    public function test_it_returns_correct_events_count() : void
    {
        $stream = $this->generateStream();

        /** @noinspection PhpUnhandledExceptionInspection */
        $stream->append(new DummyDomainEvent());
        /** @noinspection PhpUnhandledExceptionInspection */
        $stream->append(new DummyDomainEvent());
        /** @noinspection PhpUnhandledExceptionInspection */
        $stream->append(new DummyDomainEvent());

        $this->assertEquals(3, $stream->count());
    }

    public function test_it_gets_new_events_correctly() : void
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $stream = $this->generateStream(
            null,
            null,
            [new DummyDomainEvent(), new DummyDomainEvent()],
            2
        );

        /** @noinspection PhpUnhandledExceptionInspection */
        $stream->append(new DummyDomainEvent());
        /** @noinspection PhpUnhandledExceptionInspection */
        $stream->append(new DummyDomainEvent());
        /** @noinspection PhpUnhandledExceptionInspection */
        $stream->append(new DummyDomainEvent());

        $this->assertCount(3, $stream->newEvents());
        // getter should not change anything
        $this->assertCount(3, $stream->newEvents());
    }

    public function test_simple_getters() : void
    {
        $stream = $this->generateStream(
            $id = (new UuidIdentifier())->toString(),
            $name = 'foo',
            $events = [],
            $version = 4
        );

        $this->assertEquals($id, $stream->id());
        $this->assertEquals($name, $stream->name());
        $this->assertEquals($version, $stream->version());
    }

    private function generateStream(
        ?string $id = null,
        ?string $name = null,
        ?array $events = null,
        ?int $version = 0
    ) : EventStream {
        /** @noinspection PhpUnhandledExceptionInspection */
        $stream = new DomainEventStream(
            $id ?? (new UuidIdentifier())->toString(),
            $name ?? User::class,
            $events ?? [],
            $version ?? 0
        );

        return $stream;
    }
}
