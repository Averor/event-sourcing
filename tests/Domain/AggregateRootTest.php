<?php declare(strict_types=1);

namespace Averor\MessageBus\EventSourcing\Tests\Domain;

use Averor\CqrsBundle\Domain\UuidIdentifier;
use Averor\MessageBus\EventSourcing\Domain\DomainEventStream;
use Averor\MessageBus\EventSourcing\Exception\AggregateMethodNotFoundException;
use Averor\MessageBus\EventSourcing\Tests\Fixtures\Domain\User;
use Averor\MessageBus\EventSourcing\Tests\Fixtures\Domain\UserCreated;
use Averor\MessageBus\EventSourcing\Tests\Fixtures\Domain\UserStatusChanged;
use PHPUnit\Framework\TestCase;

/**
 * Class AggregateRootTest
 *
 * @package Averor\MessageBus\EventSourcing\Tests\Domain
 * @author Averor <averor.dev@gmail.com>
 */
class AggregateRootTest extends TestCase
{
    public function test_it_applies_raised_events() : void
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $user = User::create(
            $id = new UuidIdentifier(),
            $name = 'John Doe',
            $email = 'johndoe@example.com'
        );

        $this->assertEquals($id->toString(), $user->getIdentifier()->toString());
        $this->assertEquals($name, $user->name());
        $this->assertEquals($email, $user->email());

        $user->activate();
        $this->assertEquals('active', $user->status());

        $user->deactivate();
        $this->assertEquals('inactive', $user->status());
    }

    public function test_it_pulls_raised_events_correctly() : void
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $user = User::create(
            $id = new UuidIdentifier(),
            $name = 'John Doe',
            $email = 'johndoe@example.com'
        );

        $user->activate();
        $user->deactivate();

        $this->assertCount(3, $user->pullEvents());

        // pulled above, now it should be empty...
        $this->assertEmpty($user->pullEvents());
    }

    /** @depends test_it_pulls_raised_events_correctly */
    public function test_it_clears_raised_events() : void
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $user = User::create(
            $id = new UuidIdentifier(),
            $name = 'John Doe',
            $email = 'johndoe@example.com'
        );

        $user->activate();
        $user->deactivate();

        $user->clearEvents();

        $this->assertEmpty($user->pullEvents());
    }

    /** @depends test_it_applies_raised_events */
    public function test_it_reconstitutes_from_event_stream() : void
    {
        $id = new UuidIdentifier();

        /** @noinspection PhpUnhandledExceptionInspection */
        $stream = new DomainEventStream(
            $id->toString(),
            User::class,
            [
                new UserCreated($id, $name = 'John Doe', $email = 'johndoe@example.com', 'new'),
                new UserStatusChanged($id, $status = 'banned')
            ],
            2
        );

        /** @noinspection PhpUnhandledExceptionInspection */
        /** @var User $user */
        $user = User::reconstitute($stream);
        $this->assertEquals($id->toString(), $user->getIdentifier()->toString());
        $this->assertEquals($name, $user->name());
        $this->assertEquals($email, $user->email());
        $this->assertEquals($status, $user->status());
    }

    public function test_it_throws_exception_when_no_apply_method_exists() : void
    {
        $this->expectException(AggregateMethodNotFoundException::class);

        /** @noinspection PhpUnhandledExceptionInspection */
        $user = User::create(
            $id = new UuidIdentifier(),
            $name = 'John Doe',
            $email = 'johndoe@example.com'
        );

        $user->rename('Jane Doe');
        $this->assertEquals('John Doe', $user->name());
    }
}
