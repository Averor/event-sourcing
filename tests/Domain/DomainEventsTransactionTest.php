<?php declare(strict_types=1);

namespace Averor\MessageBus\EventSourcing\Tests\Domain;

use Averor\CqrsBundle\Domain\UuidIdentifier;
use Averor\MessageBus\EventSourcing\Contract\DomainEvent;
use Averor\MessageBus\EventSourcing\Domain\DomainEventsTransaction;
use Averor\MessageBus\EventSourcing\Tests\Fixtures\Dummy\DummyAggregateRoot;
use Averor\MessageBus\EventSourcing\Tests\Fixtures\Dummy\DummyDomainEvent;
use PHPUnit\Framework\TestCase;

/**
 * Class DomainEventsTransactionTest
 *
 * @package Averor\MessageBus\EventSourcing\Tests\Domain
 * @author Averor <averor.dev@gmail.com>
 */
class DomainEventsTransactionTest extends TestCase
{
    public function test_it_allows_iterating_over_events() : void
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $transaction = new DomainEventsTransaction(
            (new UuidIdentifier())->toString(),
            DummyAggregateRoot::class,
            3,
            [new DummyDomainEvent(), new DummyDomainEvent(), new DummyDomainEvent()]
        );

        $count = 0;
        foreach ($transaction as $event) {
            $count++;
            $this->assertInstanceOf(DomainEvent::class, $event);
        }

        $this->assertEquals(3, $count);
    }

    public function test_it_returns_correct_events_count() : void
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $transaction = new DomainEventsTransaction(
            (new UuidIdentifier())->toString(),
            DummyAggregateRoot::class,
            3,
            [new DummyDomainEvent(), new DummyDomainEvent(), new DummyDomainEvent()]
        );

        $this->assertEquals(3, $transaction->count());
    }

    public function test_to_array_returns_correct_events_array() : void
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $transaction = new DomainEventsTransaction(
            (new UuidIdentifier())->toString(),
            DummyAggregateRoot::class,
            3,
            [$e1 = new DummyDomainEvent(), $e2 = new DummyDomainEvent(), $e3 = new DummyDomainEvent()]
        );

        $this->assertEquals(
            [$e1, $e2, $e3],
            $transaction->toArray()
        );
    }
}
