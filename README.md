# Averor/Event-Sourcing

[![pipeline status](https://gitlab.com/Averor/event-sourcing/badges/master/pipeline.svg)](https://gitlab.com/Averor/event-sourcing/commits/master)
[![coverage report](https://gitlab.com/Averor/event-sourcing/badges/master/coverage.svg)](https://gitlab.com/Averor/event-sourcing/commits/master)
[![MIT License](https://img.shields.io/badge/license-MIT-brightgreen.svg)](https://gitlab.com/Averor/event-sourcing/blob/master/LICENSE)
[![Semver](http://img.shields.io/SemVer/2.0.0-pre-alpha.png)](http://semver.org/spec/v2.0.0.html)

::description goes here::

## Install using Composer:

`composer require averor/event-sourcing`

## Documentation

Will be surely written... someday. As for now - just take a look at [examples/](https://gitlab.com/Averor/event-sourcing/tree/master/examples) 
or [tests/](https://gitlab.com/Averor/event-sourcing/tree/master/tests)

## Testing with PHPUnit (>=7.0)
`$ ./vendor/bin/phpunit ./tests` 
